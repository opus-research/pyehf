import plyj.model as m

""" This module contains the basic Java structures (variables, methods and classes) that
    compose a Java program.
"""
__author__ = 'Benjamin Jakobus'


class JavaClass:
    """ Encapsulates the concept of a Java class.
    """
    def __init__(self, name=None, file_path=None):
        self.parent_class = None
        self.visibility = None
        self.methods = []
        self.data_members = []
        self.name = name
        self.inner_classes = []
        self.imports = []
        self.package = None
        self.file_path = file_path
        self.compartment_id = None
        self.static_initializers = []


    def visualize(self, tab='\t'):
        print tab + self.name
        print tab + '\tNumber of inner classes:' + str(len(self.inner_classes))
        print tab + '\tData members'
        for dm in self.data_members:
            print tab + '\t\t' + dm.type + ' ' + dm.name
        for c in self.inner_classes:
            c.print_result(tab + '\t')


class JavaStaticInitializer(object):
    """ Encapsulates the concept of a static initializer

    """
    def __init__(self):
         # The exceptions thrown by this method
        self.throw = []

        # The exceptions caught by this method
        self.handle = []

        # The exceptions re-mapped by this method
        # "A remap occurs when a throw statement within a catch block throws
        # an exception instance different from the instance caught by the catch block" - Eiji Adachi
        self.remap = []

        # Exceptions re-thrown by this method.
        # "A rethrows occurs when the same instance caught by the catch
        # block is used as the argument of the throw statement" - Eiji Adachi
        self.rethrow = []

        # The exceptions that are propagates by this method
        self.propagate = []

        self.name = '{}'

        self.compartment_id = None

    def to_str(self):
        return '{}'


class JavaVariable(object):
    """ Encapsulates the concept of a (Java) variable.
        Consists of two data members: the variable name and the variable type.
    """
    def __init__(self, name=None, type=None):
        self.name = name
        self.type = type


class JavaMethod:
    def __init__(self, name, args, modifiers, return_type):
        """ This is an encapsulation of a Java method. It is used by
            the JavaClass object to encapsulate method information

            Keyword arguments:
            name        --  (String) The name of the method
            args        -- (List) List of FormalParameter objects that represent the arguments passed to the
                            method
            modifiers   -- (List) List of modifiers
            return_type -- The method's return type
        """
        from search_engine.util import get_method_parameters, get_value

        self.name = name
        self.variables = []
        # Loops (such as for-loops) contained within the method. block signature => [JavaVariable]
        self.loops = {}

        # The exceptions thrown by this method
        self.throw = []

        # The exceptions caught by this method
        self.handle = []

        # The exceptions re-mapped by this method
        # "A remap occurs when a throw statement within a catch block throws
        # an exception instance different from the instance caught by the catch block" - Eiji Adachi
        self.remap = []

        # Exceptions re-thrown by this method.
        # "A rethrows occurs when the same instance caught by the catch
        # block is used as the argument of the throw statement" - Eiji Adachi
        self.rethrow = []

        # The exceptions that are propagates by this method
        self.propagate = []

        # The ID of the compartment to which this method belongs
        self.compartment_id = None

        # The class to which this method belongs
        self.parent = None

        # Extract only the type information from the parameters
        self.args = get_method_parameters(args)

        self.modifiers = modifiers

        if type(return_type) is str:
            self.return_type = return_type
        elif type(return_type) is m.Type:
            self.return_type = get_value(return_type.name)
        else:
            self.return_type = None

    def to_str(self):
        """ Convert a method into a string of the
            form 'method_name(arg0, arg1, ..., argn)'
        """
        str = '('
        for arg in self.args:
            str += arg.type + ','
        str = str.rstrip(',')
        str += ')'
        return self.name + str

