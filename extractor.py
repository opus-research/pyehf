import os
import plyj.parser as plyj

__author__ = 'Benjamin Jakobus'

# Class for recursively extracting file contents, producing two
# dictionaries: <File name, Tree>
class Extractor:

    def __init__(self):
        # Hashmap <File path, Contents>
        self.files = {}

    def walk(self, root):
        parser = plyj.Parser()
        for dir_name, sub_dir_list, file_list in os.walk(root):
            for fname in file_list:
                if not fname.endswith('.java'):
                    continue
                with open(os.path.abspath(os.path.join(dir_name, fname)), 'r') as file:
                    self.files[dir_name + '/' + fname] = tree = parser.parse_string(file.read())

            for dir in sub_dir_list:
                self.walk(dir)



