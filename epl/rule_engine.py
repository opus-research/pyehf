import re
from epl.rule_validator import RuleValidator
from epl.util import extract_exceptions
from util import extract_ehf_info

__author__ = 'Benjamin Jakobus'

class RuleEngine:
    """ The rule engine is executed after the PreProcessor
        creates a map of the program that is to be parsed. It extracts and parses
        the EHF rules, assigns compartment ids to each JavaClass and
        JavaMethod object produced by the PreProcessor and searches for rule violations.
        It also maintains a dictionary of compartment IDs in the form of:
        compartment_id => [ x where x is of type is JavaClass or JavaMethod ]
    """

    def __init__(self, program):
        # Add your new rules here. The rule_set takes the form of [(regular expression, function fn)] whereby
        # fn is a function that accepts the regex result as input.
        self.definitions = [
            ("define\s+([a-zA-Z0-9_\.\*]+)\s+as\s+compartment\s+([a-zA-Z0-9_\.\*]+)\s*;",
             self.map_create_compartment_definition),
            ("define\s+([a-zA-Z0-9_\.,\s\*]+)\s+as\s+alias\s+([a-zA-Z0-9_\.\*]+)\s*;", self.map_alias)
        ]
        self.rule_set = [
            ("only\s+([a-zA-Z0-9_\.\*]+)\s+may\s+(handle|raise|propagate|remap|rethrow)\s+([a-zA-Z0-9_\.\*]+)\s*;",
             self.map_only_may),
            ("\s*([a-zA-Z0-9_\.\*]+)\s+must\s+(handle|raise|propagate|remap|rethrow)\s+([a-zA-Z0-9_\.\*]+)\s*;",
             self.map_must),
            ("\s*([a-zA-Z0-9_\.\*]+)\s+may\s+only\s+(handle|raise|propagate|remap|rethrow)\s+([a-zA-Z0-9_\.\*]+)\s*;",
             self.map_may_only),
            ("\s*([a-zA-Z0-9_\.\*]+)\s+cannot\s+(handle|raise|propagate|remap|rethrow)\s+([a-zA-Z0-9_\.\*]+)\s*;",
             self.map_cannot),
        ]
        self.program = program

        # {compartment_id => [JavaClass|JavaMethod]}
        self.compartment_definitions = {}

        # List of tuples in the form (str, str, JavaMethod, str)
        self.only_may_violations = []

        # List of tuples in the form (str, str, JavaMethod, str)
        self.must_violations = []

        # List of tuples in the form (str, str, JavaMethod, str)
        self.may_only_violations = []

        # alias => [exceptions]
        self.aliases = {}

        # If you want to access the result sets for visualization, then create an empty list here and populate
        # it with your rule method. NOTE: Resultsets should not contain JSON. To map from a RuleParser resultset to
        # JSON for visualization, extend RuleParserViz in ui.report_builder.py
        #========================
        # TODO: If you want, you can add custom result sets here!!
        #========================

    def run(self, file_path):
        """ Parses the EHF file.

            Keyword arguments:
            file_path   -- (string) String pointing to the file containing the EHF definitions.
            program     -- (dict)   The program produced by parsing the source code
        """
        with open(file_path) as f:
            file_content = f.read()
            file_content = file_content.replace('\r', '')
            file_content = file_content.replace('\n', '')

            # Run rules for creating compartments and aliases
            # (should always be first!!)
            for rule in self.definitions:
                exp = re.compile(rule[0])
                result = exp.findall(file_content)
                if callable(rule[1]):
                    fn = rule[1]
                    key, rules, fn2 = fn(result)
                    fn2(rules)

            # Apply rules
            ehf_rules = {}
            fn_rules = []
            for rule in self.rule_set:
                exp = re.compile(rule[0])
                result = exp.findall(file_content)
                if callable(rule[1]):
                    fn = rule[1]
                    key, rules, fn2 = fn(result)
                    ehf_rules[key] = rules
                    fn_rules.append((fn2, rules))

            # Validate rules
            rv = RuleValidator()
            if not rv.run(ehf_rules):
                print '\033[91mRUN FAIL. Rules are inconsistent.\033[92m'
                return

            print 'Validation complete. No inconsistencies.\n'

            # Apply rules
            for item in fn_rules:
                fn = item[0]
                fn(item[1])

    def map_create_compartment_definition(self, rules):
        return 'def', rules, self.create_compartment_definition

    def map_only_may(self, rules):
        return 'only-may', rules, self.only_may

    def map_must(self, rules):
        return 'must', rules, self.must

    def map_may_only(self, rules):
        return 'may-only', rules, self.may_only

    def map_cannot(self, rules):
        return 'cannot', rules, self.cannot

    def map_alias(self, rules):
        return 'alias', rules, self.create_alias

    def create_alias(self, result):
        """ Populates the aliases dictionary with aliases and their
            corresponding exception types.

            Keyword Arguments:
            result      -- List of tuples (of the form ('exception1, exception2', 'alias))
        """
        for r in result:
            exceptions = extract_exceptions(r[0])
            alias = str(r[1]).strip()
            self.aliases[alias] = exceptions

    def create_compartment_definition(self, result):
        """ Assigns compartment IDs to the program elements.

            Keyword arguments:
            result  --  (list) List of tuples containing matched sub-expressions in the form of
                        (class name | method name, compartment_id)
        """
        for package, classes in self.program.iteritems():
            for c in classes:
                for item in result:
                    fully_qualified_class_name = c.package + '.' + c.name
                    compartment_def = item[0]
                    method_name = compartment_def.split('.')
                    method_name = method_name[len(method_name) - 1]

                    if method_name is not '*':
                        compartment_def = compartment_def.rpartition('.')[0]
                    else:
                        method_name = '.*'
                    compartment_def = compartment_def.replace('.', '\.')
                    compartment_def = compartment_def.replace('*', '.*')
                    compartment_def = compartment_def.replace('\..*', '.*')
                    compartment_id = item[1]
                    exp = re.compile(compartment_def)

                    method_exp = re.compile(method_name)

                    if exp.match(fully_qualified_class_name) is not None:
                        print 'match'
                        print fully_qualified_class_name
                        print compartment_def
                        c.compartment_id = compartment_id
                     #   if len(name_struct) == 2:
                        c.compartment_id = compartment_id
                        for si in c.static_initializers:
                            si.compartment_id = compartment_id
                        els = self.compartment_definitions.setdefault(compartment_id, [])
                        els.append(c)

                        for method in c.methods:
                            if method_exp.match(method.name) is not None:
                                method.compartment_id = compartment_id
                                els = self.compartment_definitions.setdefault(compartment_id, [])
                                els.append(method)

    def only_may(self, result):
        """ Processes the only-may EHF rule. This rule takes the form of
            only <compartment-id> may <dependency-type> <exception-list>
            whereby dependency-type must be one of the following:
            handle|raise|propagate|remap|rethrow

            Keyword arguments:
            result  --  (list) The result of applying the regexp for the rule
                        to a given statement.
        """
        violations = []
        for el in result:
            compartment_id, dependency_type, exceptions = extract_ehf_info(el, self.aliases)
            for package, classes in self.program.iteritems():
                for c in classes:
                    for method in c.methods:
                        if method.compartment_id != compartment_id:
                            violations = violations + self._only_may_dependency_check(package, c.name, method, compartment_id, exceptions,
                                                            dependency_type)
                    # Classes may contain exception handling code inside static initializers
                    for si in c.static_initializers:
                        if si.compartment_id != compartment_id:
                            violations += self._only_may_dependency_check(package, c.name, si, compartment_id, exceptions,
                                                        dependency_type)
        return violations

    def _only_may_dependency_check(self, package, class_name, method, compartment_id, exceptions, dependency_type):
        """ Processes the dependency check for the only-may rule. This method should only be called
            from only_may().

            Keyword arguments:
            package             --  (string)
            class_name          --  (string)
            method              --  (JavaMethod)
            compartment_id      --  (string)
            exceptions          --  (list)
            dependency_type     -- (string) Must be handle|raise|propagate|remap
        """
        violations = []
        # Rename because "raise" is a reserved keyword in Python
        if dependency_type == 'raise':
            dependency_type = 'throw'

        for exception in getattr(method, dependency_type):
                for ex in exceptions:
                    if re.match(ex.strip(), exception):
                        explanation = '(method ' + method.name + dependency_type + 's ' + exception + ' when it shouldn\'t)'
                        self.only_may_violations.append((package, class_name, method, explanation))
                        violations.append(method.name)
                        print class_name + ' in package ' + package + ' violates policy from ' + compartment_id
                        print '\t' + explanation + '\n\n'
        return violations

    def must(self, result):
        """ Processes the must EHF rule. This rule takes the form of
            <compartment-id> must <dependency-type> <exception-list>
            whereby dependency-type must be one of the following:
            handle|raise|propagate|remap|rethrow

            Keyword arguments:
            result  --  (list) The result of applying the regexp for the rule
                        to a given statement.
        """
        violations = []
        for el in result:
            compartment_id, dependency_type, exceptions = extract_ehf_info(el, self.aliases)
            for package, classes in self.program.iteritems():
                for c in classes:
                    for method in c.methods:
                        if method.compartment_id == compartment_id:
                            violations += self._must_dependency_check(package, c.name, method, compartment_id, exceptions,
                                                        dependency_type)

                    # Classes may contain exception handling code inside static initializers
                    for si in c.static_initializers:
                        if si.compartment_id == compartment_id:
                            violations += self._must_dependency_check(package, c.name, si, compartment_id, exceptions,
                                                        dependency_type)
        return violations

    def cannot(self, result):
        """ Processes the cannot EHF rule. This rule takes the form of
            <compartment-id> cannot <dependency-type> <exception-list>
            whereby dependency-type must be one of the following:
            handle|raise|propagate|remap|rethrow

            Keyword arguments:
            result  --  (list) The result of applying the regexp for the rule
                        to a given statement.
        """
        violations = []
        for el in result:
            compartment_id, dependency_type, exceptions = extract_ehf_info(el, self.aliases)
            for package, classes in self.program.iteritems():
                for c in classes:
                    for method in c.methods:
                        if method.compartment_id == compartment_id:
                            violations += self._cannot_dependency_check(package, c.name, method, compartment_id, exceptions,
                                                        dependency_type)

                    # Classes may contain exception handling code inside static initializers
                    for si in c.static_initializers:
                        if si.compartment_id == compartment_id:
                            violations += self._cannot_dependency_check(package, c.name, si, compartment_id, exceptions,
                                                        dependency_type)
        return violations

    def _cannot_dependency_check(self, package, class_name, method, compartment_id, exceptions, dependency_type):
        """ Processes the dependency check for the cannot rule. This method should only be called
            from cannot().

            Keyword arguments:
            package             --  (string)
            class_name          --  (string)
            method              --  (JavaMethod)
            compartment_id      --  (string)
            exceptions          --  (list)
            dependency_type     -- (string) Must be handle|raise|propagate|remap
        """
        violations = []
         # Rename because "raise" is a reserved keyword in Python
        if dependency_type == 'raise':
            dependency_type = 'throw'

        num_matches = 0
        matched = []

        if 'any' in exceptions:
            matched = getattr(method, dependency_type)
            num_matches = len(matched)
        else:
            for exception in getattr(method, dependency_type):
                for ex in exceptions:
                    if re.match(ex.strip(), exception):
                        num_matches += 1
                        if exception not in matched:
                            matched.append(exception)
                        break

        if num_matches != 0:
            explanation = '(method ' + method.name + ' cannot ' + dependency_type + ' ' + str(matched) + ')'
            self.must_violations.append((package, class_name, method, explanation))
            violations.append(method.name)
            print class_name + ' in package ' + package + ' violates policy from ' + compartment_id
            print '\t' + explanation + '\n\n'
        return violations

    def _must_dependency_check(self, package, class_name, method, compartment_id, exceptions, dependency_type):
        """ Processes the dependency check for the must rule. This method should only be called
            from only_may().

            Keyword arguments:
            package             --  (string)
            class_name          --  (string)
            method              --  (JavaMethod)
            compartment_id      --  (string)
            exceptions          --  (list)
            dependency_type     -- (string) Must be handle|raise|propagate|remap
        """
        violations = []
         # Rename because "raise" is a reserved keyword in Python
        if dependency_type == 'raise':
            dependency_type = 'throw'

        num_matches = 0
        not_matched = []
        for exception in getattr(method, dependency_type):
            matched = False
            for ex in exceptions:
                if re.match(ex.strip(), exception):
                    matched = True
                    num_matches += 1
                    break
            if matched is False:
                not_matched.append(ex.strip())

        if num_matches != len(exceptions):
            explanation = '(method ' + method.name + ' must ' + dependency_type + str(not_matched) + ')'
            self.must_violations.append((package, class_name, method, explanation))
            violations.append(method.name)
            print class_name + ' in package ' + package + ' violates policy from ' + compartment_id
            print '\t' + explanation + '\n\n'
        return violations

    def may_only(self, result):
        """ Processes the may-only EHF rule. This rule takes the form of
            <compartment-id> may only <dependency-type> <exception-list>
            whereby dependency-type must be one of the following:
            handle|raise|propagate|remap|rethrow

            Keyword arguments:
            result  --  (list) The result of applying the regexp for the rule
                        to a given statement.
        """
        violations = []
        for el in result:
            compartment_id, dependency_type, exceptions = extract_ehf_info(el, self.aliases)
            for package, classes in self.program.iteritems():
                for c in classes:
                    for method in c.methods:
                        if method.compartment_id == compartment_id:
                            violations += self._may_only_dependency_check(package, c.name, method, compartment_id, exceptions,
                                                            dependency_type)
                    # Classes may contain exception handling code inside static initializers
                    for si in c.static_initializers:
                        if si.compartment_id == compartment_id:
                            violations += self._may_only_dependency_check(package, c.name, si, compartment_id, exceptions,
                                                        dependency_type)
        return violations

    def _may_only_dependency_check(self, package, class_name, method, compartment_id, exceptions, dependency_type):
        """ Processes the dependency check for the only-may rule. This method should only be called
            from only_may().

            Keyword arguments:
            package             --  (string)
            class_name          --  (string)
            method              --  (JavaMethod)
            compartment_id      --  (string)
            exceptions          --  (list) The exceptions that may be raised|handled|remapped|propagated by this compartment
            dependency_type     -- (string) Must be handle|raise|propagate|remap
        """
        violations = []
         # Rename because "raise" is a reserved keyword in Python
        if dependency_type == 'raise':
            dependency_type = 'throw'

        for exception in getattr(method, dependency_type):
            for ex in exceptions:
                if re.match(ex.strip(), exception) is None:
                    explanation = '(method ' + method.name + dependency_type + 's ' + exception + ' when it shouldn\'t. Only' \
                          + str(exceptions) + ' may be raised)'
                    self.may_only_violations.append((package, class_name, method, explanation))
                    violations.append(method.name)
                    print class_name + ' in package ' + package + ' violates policy from ' + compartment_id
                    print '\t' + explanation + '\n\n'

        return violations


class Summary(dict):
    """ Creates a summary of  of the actual exception handling code as is in the source code.
        For example:
            CONTROLLER handles { A, B, C, D, E }
            CONTROLLER propagates { A, B, E }
    """

    def __init__(self, program):
        for package, classes in program.iteritems():
            for c in classes:
                for method in c.methods:
                    if method.compartment_id is not None:
                        self._populate(method)

                for si in c.static_initializers:
                    if si.compartment_id is not None:
                        self._populate(si)

    def _populate(self, obj):
        """ Populates the summary with the exception
            info contained within the given object

            Keyword Arguments:
            obj     -- Must be an instance of either JavaStaticInitializer or JavaMethod
        """
        key = obj.compartment_id + ' handles'
        self._add_exceptions(key, obj.handle)

        key = obj.compartment_id + ' throws'
        self._add_exceptions(key, obj.throw)

        key = obj.compartment_id + ' remaps'
        self._add_exceptions(key, obj.remap)

        key = obj.compartment_id + ' rethrows'
        self._add_exceptions(key, obj.rethrow)

    def _add_exceptions(self, key, exceptions):
        """ Merges the given exceptions with those
            mapped to the given key.

            Keyword Arguments:
            key             -- (str)
            exceptions      -- (list) List of exceptions (list of strings)
        """
        if not exceptions:
            return

        existing_exceptions = self.setdefault(key, [])
        existing_exceptions += exceptions
        self[key] = existing_exceptions

    def print_all(self):
        """ Prints the summary to stdout.

        """
        print '\033[94mSource code exception handling summary'
        print '=====================================\033[92m'

        for key, list in self.iteritems():
            print '\033[94m' + key + '\033[92m\n'
            for item in list:
                print '\t ' + item + '\n'

        print '\033[94m=====================================\033[92m'
