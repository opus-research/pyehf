from epl.util import extract_ehf_info

__author__ = 'Benjamin Jakobus'


class RuleValidator:
    """ The validator checks for rule consistency. An example of an
        inconsistency is: only A may handle X; B may only handle X;

        To date, the rule validator checks for the following inconsistencies (identified by Eiji Adachi):

        Must - Cannot: For the same compartment and the same dependency-relation type,
                exists a must-rule and a cannot-rule that have a common exception type.
                E.g. C must handle Foo, AND C cannot handle FOO

        May-Only - Cannot, and Only-May - Cannot: similar to previous definition

        Only-May - Only-May: occur when two only-may rules state that two different compartments
                    may establish a dependency-relation with the same exception type.
                    E.g. only C may raise Foo, and only Y may raise Foo

        Only-may - May-only: occur an exception is declared in a only-may rule but it is not
                    declared in a may-only rule for the same compartment.
                    E.g. only C may raise Foo, AND C may-only raise Bar;
    """
    def __init__(self, aliases={}):
        self._must_cannot_buffer = {}
        self._cannot_may_only_buffer = {}
        self._only_may_buffer = {}
        self._may_only_only_may_buffer = {}

        self.log = []
        self.aliases = aliases

    def run(self, rules):
        """ Runs the validator on the given rule set.

            Keyword arguments:
            rules       -- rule_type => [list of tuples]. The rule list consists of tuples, whereby
                        each tuple represents one rule. (compartment_id, dependency_type, exceptions)
            aliases     -- (dict) Aliases for exceptions

        """
        is_consistent = True

        if 'must' in rules and 'cannot' in rules:
            is_consistent = self._must_cannot_check(rules['must'], rules['cannot'])

        # Only check the other rules if first check didn't fail
        if is_consistent is True and 'cannot' in rules and 'may-only' in rules:
            is_consistent = self._cannot_may_only(rules['cannot'], rules['may-only'])

        if is_consistent is True and 'only-may' in rules:
            is_consistent = self._only_may(rules['only-may'])

        if is_consistent is True and 'only-may' in rules and 'may-only' in rules:
            is_consistent = self._may_only_only_may(rules['only-may'], rules['may-only'])

        return is_consistent

    def _must_cannot_check(self, must_rules, cannot_rules):
        """ Checks for an inconsistency with the must-cannot
            rule specification.

            Keyword arguments:
            must_rules    --  List of "must" rules
            cannot        -- List of "cannot" rules
        """
        for rule in must_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            for exception in exceptions:
                l = self._must_cannot_buffer.setdefault(exception, [])
                if compartment_id + dependency_type not in l:
                    l.append(compartment_id + dependency_type)

        for rule in cannot_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            for exception in exceptions:
                l = self._must_cannot_buffer.setdefault(exception, [])
                if str(compartment_id + dependency_type) in l:
                    self.log.append('ERROR: Inconsistent rules: "' + compartment_id + ' ' +
                                    dependency_type + ' ' + exception + '"')
                    return False
                else:
                    l.append(compartment_id + dependency_type)

        return True

    def _cannot_may_only(self, cannot_rules, may_only_rules):
        """ Checks for an inconsistency with the must-may-only rule
            specification.

            Keyword arguments:
            cannot_rules    -- (list) List of "must" rules
            may_only        -- (list) List of "cannot" rules
        """
        for rule in cannot_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            for exception in exceptions:
                l = self._cannot_may_only_buffer.setdefault(exception, [])
                if compartment_id + dependency_type not in l:
                    l.append(compartment_id + dependency_type)

        for rule in may_only_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            for exception in exceptions:
                l = self._cannot_may_only_buffer.setdefault(exception, [])
                if compartment_id + dependency_type in l:
                    self.log.append('ERROR: Inconsistent rule: ' + compartment_id + ' '
                                    + dependency_type + ' ' + exception)
                    return False
                else:
                    l.append(compartment_id + dependency_type)

        return True

    def _only_may(self, only_may_rules):
        """ Checks for an inconsistency with the must-may-only rule
            specification.

            Keyword arguments:
            only_may_rules    -- (list) List of "only-may" rules
        """
        for rule in only_may_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            for exception in exceptions:
                if exception in self._only_may_buffer:
                    if self._only_may_buffer[exception] == dependency_type:
                        self.log.append('ERROR: Inconsistent rule: ' + compartment_id + ' '
                                    + dependency_type + ' ' + exception)
                        return False

                self._only_may_buffer[exception] = dependency_type
        return True

    def _may_only_only_may(self, only_may_rules, may_only_rules):
        """ Checks for an inconsistency with the must-may-only rule
            specification.

            Keyword arguments:
            only_may_rules    -- (list) List of "only-may" rules
            only_may_rules    -- (list) List of "may_only_rules" rules
        """
        for rule in may_only_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            d = self._may_only_only_may_buffer.setdefault(compartment_id, {})
            l = d.setdefault(dependency_type, [])
            for exception in exceptions:
                if exception not in l:
                    l.append(exception)

        for rule in only_may_rules:
            compartment_id, dependency_type, exceptions = extract_ehf_info(rule, self.aliases)
            if compartment_id in self._may_only_only_may_buffer:
                d = self._may_only_only_may_buffer[compartment_id]
                if dependency_type in d:
                    l = d[dependency_type]
                    for exception in exceptions:
                        if exception not in l:
                            self.log.append('ERROR: Inconsistent rule: ' + compartment_id + ' is not allowed to '
                                    + dependency_type + ' ' + exception + '. May only ' + dependency_type
                                            + ' ' + str(l))
                            return False
        return True