__author__ = 'Benjamin Jakobus'

""" This module contains a collection of utility methods
    that are used across the rule parser.
"""

def extract_ehf_info(tuple, aliases):
    """ Accepts as input a tuple of three elements: (compartment_id, depedency_type, exceptions)
        The tuple was produced by the regular expressions for the must, only-may and may-only rules.
        Returns the items as separate variables.

        Keyword argument:
        tuple   --  (tuple) A tuple of three elements in the form of (compartment_id, depedency_type, exceptions)
        aliases -- (dict) Map of aliases
    """
    compartment_id = tuple[0]
    dependency_type = tuple[1]
    exceptions = tuple[2].split(',')
    exceptions = [ex.replace('*', '.*').strip() for ex in exceptions]

    # If we only have one exception, make sure that it isn't an alias
    if len(exceptions) == 1:
        # print exceptions
        if exceptions[0] in aliases:
            exceptions = aliases[exceptions[0]]

    return compartment_id, dependency_type, exceptions


def extract_exceptions(ex_str):
    """ Returns a comma-separated string of exceptions and
        returns them as a list

        Keyword arguments:
        ex_str     -- Comma-separated string of exceptions

    """
    exceptions = ex_str.split(',')
    exceptions = [ex.replace('*', '.*').strip() for ex in exceptions]
    return exceptions