from search_engine.query_processor import QueryExecutor
from search_engine.queries.assign_exception_usage import AssignExceptionUsage
from epl.rule_engine import RuleEngine, Summary

__author__ = 'Benjamin Jakobus'

# Applies search_engine to the given project files and
# displays output
class Analyzer:

    # files: Dictionary <file path, tree>
    def __init__(self, files):
        self.files = files

    def run(self, output_dir, ehf_rules, display_summary, display_source_code_summary = False):
        se = QueryExecutor(self.files)
        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()
        aeu = AssignExceptionUsage(pre_processor_results)

        # Queries that search_engine method bodies
        # se.add_method_query(aeu)
        se.add_multitype_query(aeu)

        # Execute search_engine
        se.run()

        # Run the rule parser
        rule_parser = RuleEngine(se.program)
        rule_parser.run(ehf_rules)

        # Create a summary
        if display_summary:
            summary = Summary(se.program)
            #TODO: Refactor. Summary should not be displayed here (?)
            summary.print_all()

        # Display source code summary
        print '\033[94mSource code exception handling summary'
        print '=====================================\033[92m'
        if display_source_code_summary:
            for package, classes in se.program.iteritems():
                for c in classes:
                    self._print_class_info(c)

        print '=====================================\033[92m'


    def _print_class_info(self, java_class):
        """ Prints a summary of the given class' exception handling constructs.

            java_class      -- Instance of JavaClass

        """
        print '\033 Class name: ' + java_class.name

        for si in java_class.static_initializers:
            if len(si.throw) == 0 and len(si.remap) == 0 and len(si.rethrow) == 0 and len(si.propagate) == 0:
                continue
            if len(si.throw) > 0:
                print 'The exceptions thrown by this static initializer: ' + str(si.throw)
            if len(si.remap) > 0:
                print 'The exceptions re-mapped by this static initializer: ' + str(si.remap)
            if len(si.rethrow) > 0:
                print 'The exceptions re-thrown by this static initializer.: ' + str(si.rethrow)
            if len(si.propagate) > 0:
                print 'The exceptions propagated by this static initializer.: ' + str(si.propagate)

        for method in java_class.methods:
            if len(method.throw) == 0 and len(method.remap) == 0 and len(method.rethrow) == 0 and len(method.propagate) == 0:
                continue
            print '---'
            print 'Method: ' + method.name
            if len(method.throw) > 0:
                print 'The exceptions thrown by this method: ' + str(method.throw)
            if len(method.remap) > 0:
                print 'The exceptions re-mapped by this method: ' + str(method.remap)
            if len(method.rethrow) > 0:
                print 'The exceptions re-thrown by this method.: ' + str(method.rethrow)
            if len(method.propagate) > 0:
                print 'The exceptions propagated by this method.: ' + str(method.propagate)
            print '---'

        for inner_class in java_class.inner_classes:
            self._print_class_info(inner_class)
