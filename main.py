from extractor import *
from analyzer import *
import traceback
import time
import sys, getopt

__author__ = 'Benjamin Jakobus'

def main(argv):
    input_dir = ''
    output_dir = ''
    ehf_rules = ''
    display_summary = False
    display_source_code_summary = False

    try:
        opts, args = getopt.getopt(argv,"hi:r:s",["ifile=", "rules=", "summary", "completeSummary"])
    except getopt.GetoptError:
        print 'test.py -i <input directory> -r <path to epl ruleset>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <input directory> -r <path to epl ruleset>'
            sys.exit()
        elif opt in ("-i", "--ifile"):
            input_dir = arg
        elif opt in ("-r", "--rules"):
            ehf_rules = arg
        elif opt in ("-s", "--summary"):
            display_summary = True
        elif opt in ("-c", "--completeSummary"):
            display_source_code_summary = True

    start = time.clock()

    extractor = Extractor()
    print "Parsing files...please wait"

    extractor.walk(input_dir)

    print "Done parsing"
    print len(extractor.files.keys())
    try:
        analyzer = Analyzer(extractor.files)
        analyzer.run(output_dir, ehf_rules, display_summary, display_source_code_summary)
    except Exception as e:
        traceback.print_exc()
    elapsed = time.clock()
    elapsed = elapsed - start
    print "Elapsed time ", elapsed

if __name__ == "__main__":
   main(sys.argv[1:])
