# PyEHF #

Welcome to PyEHF! PyEHF is an experimental implementation of EPL - a DSL for specifying exception handling
 policies. PyEHF has one dependency:

* PlyJ - https://github.com/musiKk/plyj

Icon set: http://p.yusukekamiyamane.com/

### Quickstart ###
To install, type the following command into your terminal:

    sudo python setup.py install

To run PyEHF, type the following command into your terminal:

     python main.py -i /path/to/input_dir -o /path/to/output_dir --rules /path/to/rule_set.ehf


where /path/to/input_dir is the path to the directory storing the source code that you wish to analyze; /path/to/output_dir is the directory in which the resulting report should be stored (once the application terminates, a directory named "report" will appear in this output directory. Open index.html to view the report.).

To include summaries of the actual exception handling code (as is in the source code) in the output, run PyEHF with the -s
or --summary flag:

    python main.py -i /path/to/input_dir --ehf /path/to/rule_set.ehf -s

To produce a complete summary of the source code's exception handling code, run PyEHF with -c or --completeSummary flag:

    python main.py -i ~/source_code -r ~/rule_set.ehf --completeSummary

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Deployment instructions

### How do I run the Unit tests? ###
Tests are contained in the project root's \*tests\* directory. To date, it only contains tests for the rule parser (contained in
the module named \*rule_parser_tests.py\*).

#### Running the unit tests using PyCharm
Right-click on tests/rule_parser_tests.py and choose
    Run 'Unittests in rule_parser_tests'

#### Running the unit tests from the command line
Use the Unit test command:
    python -m unittest tests/rule_parser_tests

### What rules are supported by PyEHF?
Compartment definitions:

    define <classes> as compartment <compartment_id>;

"May" rule:

    only <compartment-id> may <dependency-type> <exception-list>;

"Must" rule:

    <compartment-id> must <dependency-type> <exception-list>;

"May-only" rule:

    <compartment-id> may only <dependency-type> <exception-list>;

"Cannot" rule:

    <compartment-id> cannot <dependency-type> <exception- list>;

Remapping:
**NOTE: Support for re-mapping with constraints (e.g. X may only re-map from A to B, from C to D;) will be supported soon.**

### Supported features in v1.0.0 ###
The current, and first, release of PyEHF is v1.0.0 and supports:

* Dependency relations: raise, propagate, remap, rethrow, handle
* Rule types: may-only, only-may, must, cannot
* Compartments definition with simple name-patterns (only with wildcard '*')
* Consistency check
* Aliases
* Summaries
* Syntactic sugar: any