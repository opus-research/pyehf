from search_engine.query_processor import *
from search_engine.util import *
from java_structs import *
from search_engine.util import get_value
import traceback
import hashlib

__author__ = 'Benjamin Jakobus'


class QueryExecutor:
    """ The search_engine engine is responsible for executing queries that collect information
        about the code by processing the statements produced by PlyJ. Before executing queries, the
        search_engine engine runs the pre-processor which abstracts the trees produced by PlyJ into a more
        concise data structure, producing a dictionary in the form of {package name => [JavaClass]}
        whereby [JavaClass] is a list of JavaClass objects (defined in java_structs.py) contained within
        the given package.
    """
     # files: Dictionary <file path, tree>
    def __init__(self, files):
        self.files = files
        self.queries = []

        # A reference to the program constructed by the Pre-processor. A program consists of a
        # collection of JavaClass, JavaMethod and JavaVariable objects
        self.program = None

         # i.e. list of classes => packages, list of all methods and their return type by class
        self.search = SearchAction()

    def add_method_query(self, q):
        self.search.add_method_query(q)

    def add_multitype_query(self, q):
        self.search.add_multitype_query(q)

    def add_class_query(self, q):
        self.search.add_class_query(q)

    def run_preprocessor(self):
        """ The Preprocessor creates dictionary of Java classes by applying a visitor to
            each program entity. The visitor extracts the relevant class information (method declarations,
            variable declarations etc) and encapsulates this as JavaClass objects that are then
            stored in the result_set dictionary that links packages to classes.
        """
        # The pre-processor creates a map of the entire program
        try:
            pp = PreProcessor(self.files)
            pp.run()
            self.program = pp.result_set
            # pp.print_result()
        except Exception as e:
                traceback.print_exc()
        return pp.result_set

    def run(self):
        for file_path, tree in self.files.iteritems():
            try:
                if tree is not None:
                    if tree.package_declaration is None:
                        continue
                    self.search.package_name = tree.package_declaration.name.value
                    self.search.file_path = file_path
                    tree.accept(self.search)
            except Exception as e:
                print e
                print '========'
                print file_path
                traceback.print_exc()
                continue



class CodeVisitorAction(m.Visitor):
    """ Parent class of more specific visitors that
        walk the code and apply given queries. Specifically, this class is the parent class
        for two subclasses; the pre-processor's code visitor (which produces an abstract, searchable
        representation of the code) and the search_engine engine's code visitor, which walks the code and
        applies queries to the code.
    """
    def visit_ClassDeclaration(self, class_decl):
        if class_decl.body is None:
            return
        self.class_name = class_decl.name

        for query in self.class_queries:
            query.package_name = self.package_name
            query.class_name = self.class_name
            query.apply(class_decl)

        for item in class_decl.body:
            if type(item) is m.MethodDeclaration:
                self.visit_method_declaration(item)

        return self.visit_type_declaration(class_decl)

    def visit_method_declaration(self, method_decl):
        if method_decl.body is None:
            return

        method_parameters = get_method_parameters(method_decl.parameters)

        # End refactor
        self._walk_body(method_decl.name, method_parameters, method_decl.body)
        return True

    def apply_action(self, method_name, method_parameters, statement, optional=None):
        """ Applies an action to a given statement.
            Subclasses must implement this method
        """
        print statement

    def _walk_body(self, method_name, method_parameters, statements, optional=None):
        """ Walks through the body of a method applying the search_engine queries.

            Keyword arguments:
            method_name -- (string) The name of the method being walked
            method_parameters   -- (list) The parameters passed to method_name
            statements  -- (list) List of statements to walk
            optional    -- (object) Optional reference to any object or function
        """
        if statements is None:
            return

        for statement in statements:
            self.apply_action(method_name, method_parameters, statement, optional)

            # Extract sub-statements
            if type(statement) is m.Try:
                if statement.block is not None:
                    self._walk_body(method_name, method_parameters, statement.block.statements, optional)
            elif type(statement) is m.IfThenElse:
                self._walk_if_then_else(method_name, method_parameters, statement, optional=optional)
            elif type(statement) is m.For:
                if statement.init is not None:
                    self._walk_body(method_name, method_parameters, [statement.init], optional=optional)
                if statement.body is not None:
                    if hasattr(statement.body, 'statements'):
                        self._walk_body(method_name, method_parameters, statement.body.statements, optional=optional)
                    else:
                        self._walk_body(method_name, method_parameters, [statement.body], optional=optional)
            elif type(statement) is m.While or type(statement) is m.DoWhile:
                if statement.body is not None and hasattr(statement.body, 'statements'):
                    self._walk_body(method_name, method_parameters, statement.body.statements, optional=optional)
                else:
                    self._walk_body(method_name, method_parameters, [statement.body], optional=optional)
            elif type(statement) is m.Switch:
                if statement.switch_cases is not None:
                    for case in statement.switch_cases:
                        self._walk_body(method_name, method_parameters, case.body, optional=optional)
            elif type(statement) is m.Return:
                if type(statement.result) is m.MethodInvocation:
                    if hasattr(statement.result, 'arguments'):
                        self._walk_body(method_name, method_parameters, statement.result.arguments, optional=optional)
                    else:
                        self._walk_body(method_name, method_parameters, [statement.result], optional=optional)
            elif type(statement) is m.MethodInvocation:
                self._walk_body(method_name, method_parameters, statement.arguments, optional=optional)

    def _walk_if_then_else(self, method_name, method_parameters, statement, optional=None):
        """ Walks through if-then-else statements (supports nested statements)

            Keyword arguments:
            method_name -- (string) Name of the method containing the if-then-else statement
            method_parameters -- (list) List of arguments passed to the method
            statement   -- The if-then-else statement
        """
        if statement.if_true is not None:
            if hasattr(statement.if_true, 'statements'):
                self._walk_body(method_name, method_parameters, statement.if_true.statements, optional=optional)
            else:
                self._walk_body(method_name, method_parameters, [statement.if_true], optional=optional)
        if statement.if_false is not None:
            if type(statement.if_false) is m.IfThenElse:
                self._walk_if_then_else(method_name, method_parameters, statement.if_false, optional=optional)
            elif type(statement.if_false) is m.Block:
                self._walk_body(method_name, method_parameters, statement.if_false.statements, optional=optional)

class SearchAction(CodeVisitorAction):
    """ A search_engine action is a container for search_engine queries
        executed by the search_engine engine once the pre-processor has been
        executed.
    """
    def __init__(self):
        super(CodeVisitorAction, self).__init__()
        self.method_queries = []
        self.class_queries = []
        self.package_name = ''
        self.file_path = None

        # Name of the class that is being visited
        self.class_name = ''

    def add_method_query(self, h):
        """ Adds a new method query. Method queries are
            executed only on method bodies (and not on method declarations
            or class definitions).

            Keyword arguments:
            h       -- (base_query)
        """
        self.method_queries.append(h)

    def add_class_query(self, h):
        """ Adds a new class query. Class queries are
            executed on the entire class declaration.

            Keyword arguments:
            h       -- (base_query)
        """
        self.class_queries.append(h)

    def add_multitype_query(self, h):
        """ Adds a new multi-type query. Multi-type queries are
            executed on both the class declaration and method bodies.

            Keyword arguments:
            h       -- (base_query)
        """
        self.class_queries.append(h)
        self.method_queries.append(h)

    def apply_action(self, method_name, method_parameters, statement, optional=None):
        # Call custom behaviour on statements
        for query in self.method_queries:
            try:
                query.package_name = self.package_name
                query.class_name = self.class_name
                query.current_class = query.get_class(self.class_name, self.package_name)
                query.method_name = method_name
                query.method_parameters = method_parameters
                query.file_path = self.file_path
                query.apply(statement)
            except Exception as e:
                traceback.print_exc()


class PreProcessor():
    """ The Preprocessor creates dictionary of Java classes by applying a visitor to
        each program entity. The visitor extracts the relevant class information.
        This information is aggregated in the form of JavaClass objects and then
        stored in the result_set dictionary that links packages to classes.
    """

    # Dictionary of the form package => [classes] whereby the key is the
    # full package name, the value a list of class objects contained within
    # this package
    result_set = {}

    def __init__(self, files):
        """
            Keyword arguments:
            files   - (dict) file path => tree
        """
        self.files = files

    def run(self):
        for path, tree in self.files.iteritems():
            if tree is None or tree.package_declaration is None:
                continue

            visitor = PreProcessorAction()

            try:
                visitor.imports = [decl.name.value for decl in tree.import_declarations]
                visitor.result_set = self.result_set
                visitor.package = tree.package_declaration.name.value
                visitor.file_path = path
                tree.accept(visitor)
            except Exception as e:
                # Interfaces and abstract methods throw exceptions when
                # being parsed by plyj
                traceback.print_exc()
                continue

    def print_result(self):
        for k, v in self.result_set.iteritems():
            print k
            for c in v:
                c.print_result()



class PreProcessorAction(CodeVisitorAction):
    """ Implementation of the code visitor used by the pre-processor
        to construct a representation of the code base.
    """
    imports = None
    result_set = None
    package = None
    file_path = None

    def __init__(self):
        super(PreProcessorAction, self).__init__()

    def visit_ClassDeclaration(self, class_decl):
        # Process the class declaration
        try:
            java_class = self._process_class_declaration(class_decl)
        except Exception as e:
            print e
            traceback.print_exc()
            exit()
        # Set import declarations
        java_class.imports = self.imports
        java_class.package = self.package

        # The package declaration acts as the key
        list = self.result_set.setdefault(self.package, [])
        list.append(java_class)

    def apply_action(self, method_name, method_parameters, statement, java_method):

        if type(statement) is m.VariableDeclaration:
            var = JavaVariable()
            if type(statement.type) is str:
                var.type = statement.type
            elif type(statement.type) is m.Type:
                var.type = get_value(statement.type.name)

            for var_decl in statement.variable_declarators:
                var.name = var_decl.variable.name

            java_method.variables.append(var)
        elif type(statement) is m.For:
            # Generate loop signature (this is used as a key)
            sig = hashlib.sha224(str(statement)).hexdigest()
            var_name = ''
            var_type = None

            if statement.init is not None and hasattr(statement.init, 'variable_declarators'):
                for var_decl in statement.init.variable_declarators:
                    if type(var_decl.variable) is m.Variable:
                        var_name = var_decl.variable.name

            if type(statement.init) is m.VariableDeclaration:
                if type(statement.init.type) is m.Type:
                    var_type = get_value(statement.init.type.name)
                else:
                    var_type = statement.init.type

            java_method.loops[sig] = JavaVariable(var_name, var_type)

    def _process_class_declaration(self, class_decl):
        """ Processes a class declaration. The method creates a new JavaClass object, populates it
            and calls itself if the given class declaration contains inner classes.

            Keyword arguments:
            class_decl      -- The class declaration

            Return:
            JavaClass object

        """
        if class_decl.body is None:
            return

        java_class = JavaClass(name=class_decl.name, file_path=self.file_path)

        if class_decl.extends is not None:
            java_class.parent_class = class_decl.extends.name.value

        for statement in class_decl.body:
            if type(statement) is m.ClassDeclaration:
                java_class.inner_classes.append(self._process_class_declaration(statement))
            elif type(statement) is m.MethodDeclaration:
                java_method = JavaMethod(statement.name, statement.parameters, statement.modifiers, statement.return_type)

                # Assign list of exceptions propagated by this method
                if statement.throws is not None:
                    if statement.throws.types is not None:
                        java_method.propagate = [ex.name.value for ex in statement.throws.types]

                java_method.parent = java_class
                java_class.methods.append(java_method)
                # Process method content
                self._walk_body(statement.name, java_method.args,  statement.body, java_method)
            elif type(statement) is m.FieldDeclaration:
                field = JavaVariable()
                if type(statement.type) is str:
                    field.type = statement.type
                elif type(statement.type) is m.Type:
                    field.type = get_value(statement.type.name)

                for var_decl in statement.variable_declarators:
                    field.name = var_decl.variable.name
                java_class.data_members.append(field)
        return java_class