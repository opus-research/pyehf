from java_structs import *


""" This module contains a collection of utility methods
    that are used across the search_engine package.
"""
__author__ = 'Benjamin Jakobus'


def get_value(field):
    """ Returns the value of a given field. Sometimes fields are either a string
        or of type m.Name

        Keyword arguments:
        field       -- (Name or string) The field object who's value to extract.
    """
    if type(field) is str:
        return field
    else:
        return field.value


def get_type_value(field):
    """ Returns the value associated with a Type object.
        Keyword arguments:
        args    -- (Type) The type object who's value to extract.
    """
    if type(field.name) is str:
        return field.name
    else:
        return field.name.value


def get_method_parameters(args):
    """ Takes as input a set of method arguments (extracted from the method declaration)
        and returns a list of JavaVariable objects that encapsulate the argument's type
        and name.

        Keyword arguments:
        args    -- (list) List of method parameters.
    """
    arg_types = []
    for arg in args:
        if type(arg) is m.FormalParameter:
            t = arg.type
            var = JavaVariable()
            var.name = arg.variable.name
            if type(t) is str:
                var.type = t
            elif type(t) is m.Type:
                var.type = get_value(t.name)

            arg_types.append(var)
    return arg_types