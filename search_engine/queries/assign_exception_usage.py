from java_structs import JavaStaticInitializer
import plyj.model as m

from search_engine.queries.base_query import BaseQuery
import java_structs as structs

__author__ = 'Benjamin Jakobus'


class AssignExceptionUsage(BaseQuery):
    """ This query walks method bodies, identifying exception usage (throws, catches e.t.c.) and assigns this
        information to the classes extracted by the search_engine engine's pre-processor.
    """

    def __init__(self, meta_data):
        self.num_ex_caught = 0
        self.num_ex_mapped = 0
        self.num_ex_rethrown = 0
        BaseQuery.__init__(self, meta_data)

    def apply(self, statement, is_initializer=False):
        """ Applies the heuristic of exception type to the given statement

            Keyword arguments:
            package_name    --  (string) Name of the package in which this class is contained
            is_initializer  --  (bool) True if the statement is contained inside a static initializer;
                                False if not.
        """
        if statement is None:
            return
        if type(statement) is m.ClassDeclaration:
            if hasattr(statement, 'body'):
                for s in statement.body:
                    self.apply(s, is_initializer=True)
        if type(statement) is m.ClassInitializer:
            if hasattr(statement, 'block') and statement.block.statements is not None:
                self._walk_body(statement.block.statements)

        java_class = self.get_class(self.class_name)
        java_method = self.get_method(java_class, self.method_name, self.method_parameters)

        # This happens when the exception handling happens inside a
        # static initializer
        if java_method is None or is_initializer is True:
            java_method = JavaStaticInitializer()

        if type(statement) is m.Try:
            if statement.catches is not None:
                for exception in statement.catches:
                    exception_type = exception.types[0].name.value
                    # Strip types off package information. e.g. java.io.IOException should be IException
                    if '.' in exception_type:
                        package_info = exception.types[0].name.value.rsplit('.', 1)[0] + '.'
                        exception_type = exception_type.replace(package_info, '')

                    internal_throw = False

                    if exception.block is not None:
                        block = exception.block
                        if block.statements is not None:
                            internal_throw = self._extract_mapping(block.statements, java_method, exception_type, exception.variable.name)

                    if internal_throw is False:
                        java_method.handle.append(exception_type)
                        self.num_ex_caught += 1

        elif type(statement) is m.Throw:
            exception = self.get_type(statement.exception, java_method)

            if exception is not None:
                java_method.throw.append(exception)
                if type(java_method) is structs.JavaStaticInitializer:
                    java_class.static_initializers.append(java_method)

    def _walk_body(self, statements):
        """ Walks through the body of a method applying the search_engine queries.

            Keyword arguments:
            statements  -- (list) List of statements to walk
        """
        if statements is None:
            return

        for statement in statements:
            self.apply(statement, is_initializer=True)
            # Extract sub-statements
            if type(statement) is m.Try:
                if statement.block is not None:
                    self._walk_body(statement.block.statements)
            elif type(statement) is m.IfThenElse:
                self._walk_if_then_else(statement)
            elif type(statement) is m.For:
                if statement.init is not None:
                    self._walk_body([statement.init])
                if statement.body is not None:
                    if hasattr(statement.body, 'statements'):
                        self._walk_body(statement.body.statements)
                    else:
                        self._walk_body([statement.body])
            elif type(statement) is m.While or type(statement) is m.DoWhile:
                if statement.body is not None and hasattr(statement.body, 'statements'):
                    self._walk_body(statement.body.statements)
                else:
                    self._walk_body([statement.body])
            elif type(statement) is m.Switch:
                if statement.switch_cases is not None:
                    for case in statement.switch_cases:
                        self._walk_body(case.body)
            elif type(statement) is m.Return:
                if type(statement.result) is m.MethodInvocation:
                    if hasattr(statement.result, 'arguments'):
                        self._walk_body(statement.result.arguments)
                    else:
                        self._walk_body([statement.result])
            elif type(statement) is m.MethodInvocation:
                self._walk_body(statement.arguments)

    def _walk_if_then_else(self, statement):
        """ Walks through if-then-else statements (supports nested statements)

            Keyword arguments:
            statement   -- The if-then-else statement
        """
        if statement.if_true is not None:
            if hasattr(statement.if_true, 'statements'):
                self._walk_body(statement.if_true.statements)
            else:
                self._walk_body([statement.if_true])
        if statement.if_false is not None:
            if type(statement.if_false) is m.IfThenElse:
                self._walk_if_then_else(statement.if_false)
            elif type(statement.if_false) is m.Block:
                self._walk_body(statement.if_false.statements)

    def _extract_mapping(self, statements, java_method, caught_exception_type, caught_exception_name):
        """ Checks the statements to see if a new exception
            is thrown. If yes, then it is added to the given java_method object

            Keyword arguments:
            statements              -- (list) List of statements
            java_method             -- (JavaMethod)
            caught_exception_type   -- (string) Type of the exception that is thrown
            caught_exception_name   -- (string)
        """
        throw = False
        for statement in statements:
            if type(statement) is m.Throw:
                throw = True

                exception = self.get_type(statement.exception, java_method)

                if exception is None:
                    exception = caught_exception_type

                if type(statement.exception) is m.InstanceCreation:
                    java_method.remap.append(exception)
                    self.num_ex_mapped += 1
                else:
                    java_method.rethrow.append(exception)
                    self.num_ex_rethrown += 1
        return throw


    def get_type(self, statement, java_method):
        """ Accepts an instance creation statement as a parameter
            (e.g. InstanceCreation(type=Type(name=Name(value='XYZException')) ... )
            and returns its type or None if the statement could not be parsed.

            Keyword arguments:
            statement   --
            java_method -- (JavaMethod) The method that is currently being analyzed.
        """
        if type(statement) is m.InstanceCreation:
            t = statement.type
            if type(t.name) is m.Name:
                return t.name.value
            else:
                return t.name
        else:
            # Resolve variable type
            var_name = None
            if type(statement) is m.Name:
                var_name = statement.value
            else:
                var_name = statement

            for var in java_method.variables:
                if var.name == var_name:
                    return var.type

            return self.get_member_type(self.get_class(self.class_name), var_name)

        return None



