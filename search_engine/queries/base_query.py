import plyj.model as m

""" This module contains the base representation for queries applied by
    the search_engine engine to extract various code properties.
"""
__author__ = 'Benjamin Jakobus'


class BaseQuery():

    # The name of the package in which the class that is currently being queried resides
    package_name = None
    # The name of the class that is currently being queried
    class_name = None
    # The name of the method that is currently being queried
    method_name = None
    # The parameters of the method that is currently being queried
    method_parameters = []
    # Path to the file in which the code that is currently being queried is located
    file_path = None

    def __init__(self, meta_data):
        # The pre-processor's output data
        self.meta_data = meta_data

    def apply(self, statement):
        raise NotImplementedError

    def get_result_as_json(self):
        """ Converts and returns the result set as JSON.
            Returns empty list by default.
        """
        return []

    def get_result_as_str_list(self):
        """ Returns the result items as a list of strings.
            Returns empty list by default.
        """
        return []

    @staticmethod
    def _calculate_graph_dimensions(nodes):
        if len(nodes) <= 0:
            return

        node_size = 20000 / len(nodes)
        edge_width = 1 / len(nodes) * 2
        if len(nodes) > 30:
            font_size = 10.0 / len(nodes) * 50
        elif len(nodes) > 10:
            font_size = 10.0 / len(nodes) * 20
        else:
            font_size = 8
        return node_size, edge_width, font_size

    def get_result(self):
        return self.result_set

    def print_result(self):
        print(self.result_set)

    def get_class(self, class_name=None, package_name=None):
        """ Returns the class object from result_set (obtained through the pre-processor) that represents the class
            denoted by class_name contained in class_name.

            Returns None if the class is not found.
        """
        if package_name is None:
            package_name = self.package_name


        if class_name is None or class_name == 'this':
            class_name = self.class_name

        if package_name not in self.meta_data.keys():
            return None

        classes = self.meta_data[package_name]
        if not classes:
            return None

        for c in classes:
            if c.name == class_name:
                return c

        # If nothing has been found, search_engine classes in this package for inner classes
        for c in classes:
            for inner_class in c.inner_classes:
                if inner_class.name == class_name:
                    return inner_class

        return None

    def get_member_type(self, java_class, member_name):
        """ Looks up the type of the given member by first looking
            in the given java_class, and then, if no match is found,
            searching its parents.
        """
        for member in java_class.data_members:
            if member.name == member_name:
                return member.type

        # Get parent package and parent class
        if java_class.parent_class is not None:
            return self.get_member_type(self.get_parent_class(java_class), member_name)

    def get_parent_class(self, java_class):
        """ Returns the JavaClass object that represents the
            given class' parent class. If the class does not
            extend other class, None is returned.
        """
        if java_class.parent_class is None:
            return

        parent_class_package = None
        for i in java_class.imports:
            if i.endswith(java_class.parent_class):
                parent_class_package = i[:-len('.' + java_class.parent_class)]

        if parent_class_package is None:
            parent_class_package = self.package_name

        return self.get_class(java_class.parent_class, parent_class_package)

    @staticmethod
    def get_method(java_class, method_name, parameters=None):
        """ Returns the method objects from the java_class that matches the
            given method_name and parameters. If parameters is None and
            multiple methods by the same name exist within the given class,
            then the first method matching the name is returned.
        """
        if java_class is None:
            return None

        if parameters is None:
            for method in java_class.methods:
                if method.name == method_name:
                    return method
        else:
            for method in java_class.methods:
                if method.name == method_name and set(arg.name for arg in method.args) == set(arg.name for arg in parameters):
                    return method

        return None
