import plyj.model as m
from search_engine.queries.base_query import BaseQuery
from search_engine.util import get_value, get_type_value

import hashlib

__author__ = 'Benjamin Jakobus'


# Implementation of the "heuristic of method call" proposed by
# Barbosa, Garcia and Mezini. Quoting their paper "given a code fragment C1 that calls methods m1, m2 ... mn,
# in which one of the calls mi=1...n throws an exception of type T1 and the developer wants to handle this exception.
# The Heuristic of Methods Called searches for code fragments C2 that must call mi. The more common calls C1 and C2
# share, the more relevant C2 is considered"
class QueryOfMethodCall(BaseQuery):
    # Maps variable type to list of dictionaries in the form:
    # {(object type, method invoked, path to file containing method), {class in which invocation occurs [methods in which invocation occurs]}}
    result_set = {}

    # Signature of the current block (e.g. for-loop, while-loop) etc in which
    # the query is being executed
    block_signature = None

    def __init__(self, meta_data):
        BaseQuery.__init__(self, meta_data)

    def _resolve_call_to_super(self, statement, class_list):
        """ Resolves the method call to the parent class; extracts parent class
            and adds it to the dictionary.

            Keyword arguments:
            statement       -- The statement containing the call to super()
            class_list      -- (list) List of JavaClass objects in the current package. Needed to
                                extract the JavaClass object representation of the current class
        """
        for java_class in class_list:
            if java_class.name == self.class_name:
                if java_class.parent_class is not None:
                    self._add_method_call_to_dict(statement.name, java_class.parent_class)
                    return True
        return False

    def apply(self, statement):
        """ Applies the heuristic of method calls to the given statement

            Keyword arguments:
            statement       --

        """
        if statement is None or type(statement) is str:
            return

        if type(statement) is m.VariableDeclaration:
            return

        if type(statement) is m.MethodInvocation:
            for arg in statement.arguments:
                if arg == 'this':
                    arg = self.class_name
                self.apply(arg)

            if statement.target is not None:
                if type(statement.target) is str:
                    if statement.target == 'this':
                        # Internal method call. i.e. this.fooBar()
                        self._add_method_call_to_dict(statement.name, self.class_name)
                    elif statement.target == 'super':
                        ret = self._resolve_call_to_super(statement, self.meta_data[self.package_name])
                        if not ret:
                            for c in self.meta_data[self.package_name]:
                                self._resolve_call_to_super(statement, c.inner_classes)

                elif type(statement.target) is m.Name:
                    self._resolve_and_add_method_call_to_dict(statement.name, statement.target.value)

                elif type(statement.target) is m.ArrayAccess:
                    self._resolve_and_add_method_call_to_dict(statement.name, statement.target.target.value)

                elif type(statement.target) is m.MethodInvocation:
                    # What if the target is another method invocation? We need to recursively resolve in that case
                    self._resolve_and_add_method_call_to_dict(statement.name, self._resolve_target(statement))
                elif type(statement.target) is m.Cast:
                    if type(statement.target.target) is m.Type:
                        if type(statement.target.target.name) is str:
                            self._add_method_call_to_dict(statement.name, statement.target.target)
                        elif type(statement.target.target.name) is m.Name:
                            self._add_method_call_to_dict(statement.name, get_value(statement.target.target.name))
                        elif type(statement.target.target) is str:
                            self._add_method_call_to_dict(statement.name, statement.target.target)

                elif type(statement.target) is m.ArrayAccess:
                    if type(statement.target.target) is m.MethodInvocation:
                        for arg in statement.target.target.arguments:
                            self.apply(arg)
                    elif type(statement.target.target) is m.Name:
                        self._add_method_call_to_dict(statement.name, statement.target.target.value)
                elif type(statement.target) is m.FieldAccess:
                    if type(statement.target.target) is str:
                        target = statement.target.target
                        if target == 'this':
                            target = self.class_name
                        elif target == 'super':
                            target = self.get_class().parent_class

                        self._add_method_call_to_dict(statement.name, target)
                    else:
                        self._add_method_call_to_dict(statement.name, statement.target.target.value)
            else:
                # Internal method call (within this class)
                self._add_method_call_to_dict(statement.name, self.class_name)

        if hasattr(statement, 'if_true'):
            self.apply(statement.if_true)
        if hasattr(statement, 'if_false'):
            self.apply(statement.if_false)
        if hasattr(statement, 'predicate'):
            self.block_signature = hashlib.sha224(str(statement)).hexdigest()
            self.apply(statement.predicate)
        if hasattr(statement, 'lhs'):
            self.apply(statement.lhs)
            self.apply(statement.rhs)
        if hasattr(statement, 'body'):
            self.apply(statement.body)

    def _resolve_target(self, statement):
        """ Resolves the target of a method call to a type. i.e. given a statement whose target
            is a method invocation (e.g. getFoo().getBar()), this function resolves the type
            of that target, returning it.

            Keyword arguments:
            statement       -- The statement whose target to resolve

        """
        if type(statement) is m.MethodInvocation:
            if type(statement.target) is m.MethodInvocation:
                target_type = self._resolve_target(statement.target)
                method_invoked = statement.target.name
                # Calling 'method_invoked' on object of type 'target_type'
                # Get the return type of the method that is invoked
                java_class = self.get_class()

                # We can't proceed if the class doesn't import the type. So just return the target type
                if not java_class.imports:
                    return target_type

                package_of_target_type = [package if '.' + target_type in package else None for package in java_class.imports][0]

                # If we can't fully resolve, just return the target type
                if package_of_target_type is None or package_of_target_type not in self.meta_data.keys():
                    return target_type

                package_of_target_type = package_of_target_type[:-len('.' + target_type)]

                target_classes = self.meta_data[package_of_target_type]
                for c in target_classes:
                    if c.name == target_type:
                        target_method = self.get_method(c, method_invoked)
                        return target_method.return_type

            elif type(statement.target) is m.Name:
                return self._resolve_and_add_method_call_to_dict(statement.name, statement.target.value)
            elif statement.target is None or statement.target == 'this' or statement.target == 'None':
                return self.class_name
            elif type(statement.target) is m.InstanceCreation:
                return get_type_value(statement.target.type)
            elif type(statement.target) is str:
                if statement.target == 'this':
                    return self.class_name
                elif statement.target == 'super':
                    java_class = self.get_class()
                    return java_class.parent_class
            elif type(statement.target) is m.Cast:
                return get_type_value(statement.target.target)
            elif type(statement.target) is m.FieldAccess:
                print statement.target
                java_class = self.get_class(class_name=statement.target.target)
                return self.get_member_type(java_class, statement.target.name)
            elif type(statement.target) is m.ArrayAccess:
                return self._resolve_and_add_method_call_to_dict(statement.name, get_value(statement.target.target))
            else:

                print self.class_name
                print self.package_name
                print statement
                exit()


    def _add_method_call_to_dict(self, method_invoked, target_type):
        dict = self.result_set.setdefault((target_type, method_invoked, self.file_path), {})
        list = dict.setdefault(self.class_name, [])
        list.append(self.method_name)
        return target_type

    def _resolve_and_add_method_call_to_dict(self, method_invoked, target_name):
        """ Adds the method invocation to the result set along with the type of object on
            which it is called

            Keyword arguments:
            method_invoked  -- (string) Name of the method that is being invoked
            target_name     -- (string) The name of the variable on which the method is invoked
        """

        # Use preprocessor meta data to find variable types
        # To do this, first fetch the method definition...
        java_class = self.get_class()

        java_method = self.get_method(java_class, self.method_name, self.method_parameters)

        return self._lookup_class(java_class, java_method, method_invoked, target_name)

    def _lookup_class(self, java_class, java_method, method_invoked, target_name):
        """ Looks up a given class to find the variable type of the variable denoted
            by target_name. Once found, it will add the type, along with the method that
            was invoked on it to the result set. The search_engine is performed as follows:
            first, the variables defined in java_method are searched; if we find a name that
            corresponds to the target name, then we extract its type. If nothing is found, we
            search_engine the method arguments. If once more no variable by the same name is found,
            we search_engine the class' public and private members. If that produces no results,
            we go up the inheritance tree, and search_engine the class' parent class (we go all
            the way up the inheritance tree, applying _lookup_class recursively).

            Keyword arguments:
            java_class      -- (JavaClass) Class object of the class to search_engine
            java_method     -- (JavaMethod) The method in which the variable was defined
            method_invoked  -- (string) The name of the method that was invoked on the target
            target_name     -- (string) The name of the target (variable) on which the method was invoked

        """
        #...then look up the variable type
        if java_method is not None:
            for var in java_method.variables:
                if var.name == target_name:
                    self._add_method_call_to_dict(method_invoked, var.type)
                    return var.type

            if self.block_signature is not None:
                if self.block_signature in java_method.loops.keys():
                    if java_method.loops[self.block_signature].name == target_name:
                        self._add_method_call_to_dict(method_invoked, java_method.loops[self.block_signature].type)
                        return java_method.loops[self.block_signature].type

            for arg in java_method.args:
                if arg.name == target_name:
                    self._add_method_call_to_dict(method_invoked, arg.type)
                    return arg.type

            for data_member in java_class.data_members:
                if data_member.name == target_name:
                    self._add_method_call_to_dict(method_invoked, data_member.type)
                    return data_member.type

            #...then the call is to a static method. e.g. System.out
            if java_class.parent_class is not None:
                parent_class = self.get_parent_class(java_class)

                if parent_class is not None:
                    return self._lookup_class(parent_class, None, method_invoked, target_name)
                else:
                    return self._add_method_call_to_dict(method_invoked, java_class.parent_class)
            else:
                return self._add_method_call_to_dict(method_invoked, target_name)
        else:
            for var in java_class.data_members:
                if var.name == target_name:
                    self._add_method_call_to_dict(method_invoked, var.type)
                    return var.type
        return target_name

    def get_result_as_json(self):
        # TODO: Document
        if self.result_set is None:
            return
        #{(object type, method invoked), {class in which invocation occurs [methods in which invocation occurs]}}
        json = '['
        nodes = []

        for key, callees in self.result_set.iteritems():
            class_name = key[0]
            if class_name not in nodes and len(callees) > 0:
                nodes.append(class_name)
            for callee, methods in callees.iteritems():
                if callee not in nodes:
                    # Don't add a loop. i.e. Don't create an edge when an internal method call takes place
                    if class_name != callee:
                        json += '{target:"' + class_name + '", source:"' + callee + '",type:"licensing",' \
                                                                                    'weight: ' + str(len(methods)) + '},\n'

        json += '];'

        return json

    def get_result_as_str_list(self):
        """ Returns the result set as a list of strings in the form
            of 'class name throws exception'
        """
            # {(object type, method invoked), {class in which invocation occurs [methods in which invocation occurs]}}

        list = []
        for tuple, value in self.result_set.iteritems():
            for class_name, methods in value.iteritems():
                for method in methods:
                    list.append(method + ' in ' + class_name + ' calls ' + tuple[1] + ' in ' + tuple[0])

        return list

    def print_result(self):
        print "Output takes the form of <Class name>\n\t<Classes calling methods in object above>\n"
        for key, callees in self.result_set.iteritems():
            print(str(key[0]))

            for callee in callees.keys():
                print('\t' + str(callee))
            print '\n'
