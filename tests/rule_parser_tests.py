import unittest

from epl.rule_engine import RuleEngine
from extractor import Extractor
from java_structs import JavaClass, JavaMethod
from search_engine.queries.assign_exception_usage import AssignExceptionUsage
from search_engine.query_processor import QueryExecutor


__author__ = 'Benjamin Jakobus'


class RuleParserTests(unittest.TestCase):

    def test_only_may_rule(self):
        # Only the TEST compartment may raise IOException,ABCException and FooBarException
        test_input = [('TEST_1', 'raise', 'IOException,ABCException, FooBarException')]

        # The test program
        program = {}

        test_class_1 = JavaClass()
        test_method_1 = JavaMethod('testMethod1', [], ['public'], 'void')
        test_method_1.throw = ['IOException']
        test_method_1.compartment_id = "TEST_1"
        test_method_1.remap = ['ABCException']
        test_class_1.name = 'Test1'
        test_method_1.handle = ['Exception']
        test_class_1.methods = [test_method_1]
        test_class_1.compartment_id = "TEST_1"

        test_class_2 = JavaClass()
        test_method_2 = JavaMethod('testMethod2', [], ['public'], 'void')
        test_method_2.throw = ['FooBarException']
        test_method_2.compartment_id = "TEST_2"
        test_class_2.name = 'Test2'
        test_class_2.methods = [test_method_2]
        test_class_2.compartment_id = "TEST_2"

        test_class_3 = JavaClass()
        test_method_3 = JavaMethod('testMethod3', [], ['public'], 'void')
        test_method_3.throw = ['IOException']
        test_method_3.compartment_id = "TEST_3"
        test_method_3.propagate = ['ABCException']
        test_class_3.name = 'Test3'
        test_class_3.methods = [test_method_3]
        test_class_3.compartment_id = "TEST_3"

        test_class_4 = JavaClass()
        test_method_4 = JavaMethod('testMethod4', [], ['private'], 'String')
        test_method_4.handle = ['Exception']
        test_method_4.compartment_id = "TEST_4"
        test_class_4.name = 'Test4'
        test_class_4.methods = [test_method_4]
        test_class_4.compartment_id = "TEST_4"

        program['com.test.package'] = [test_class_1, test_class_2, test_class_3, test_class_4]

        rp = RuleEngine(program)
        violations = rp.only_may(test_input)

        self.failUnless('testMethod2' in violations)
        self.failUnless('testMethod3' in violations)

        # Only TEST_3 may map ABCException;
        test_input = [('TEST_3', 'remap', 'ABCException, '),
                      ('TEST_2', 'propagate', 'ABCException, FooBarException')]

        violations = rp.only_may(test_input)

        self.failUnless('testMethod1' in violations)
        self.failUnless('testMethod3' in violations)

        # Only TEST_3 may handle Exception
        test_input = [('TEST_3', 'handle', 'Exception')]

        violations = rp.only_may(test_input)

        self.failUnless('testMethod4' in violations)

    def test_must_rule(self):
        # The TEST compartment must handle IOException,ABCException and FooBarException
        test_input = [('TEST_1', 'handle', 'IOException,ABCException, FooBarException'),
                      ('TEST_4', 'propagate', 'ArrayIndexOutOfBoundsException')]

        # The test program
        program = {}

        test_class_1 = JavaClass()
        test_method_1 = JavaMethod('testMethod1', [], ['public'], 'void')
        test_method_1.throw = ['IOException']
        test_method_1.compartment_id = "TEST_1"
        test_method_1.remap = ['ABCException']
        test_class_1.name = 'Test1'
        test_method_1.handle = ['ABCException']
        test_class_1.methods = [test_method_1]
        test_class_1.compartment_id = "TEST_1"

        test_class_2 = JavaClass()
        test_method_2 = JavaMethod('testMethod2', [], ['public'], 'void')
        test_method_2.throw = ['FooBarException']
        test_method_2.propagate = ['ABCException', 'FooBarException']
        test_method_2.compartment_id = "TEST_2"
        test_class_2.name = 'Test2'
        test_class_2.methods = [test_method_2]
        test_class_2.compartment_id = "TEST_2"

        test_class_3 = JavaClass()
        test_method_3 = JavaMethod('testMethod3', [], ['public'], 'void')
        test_method_3.throw = ['IOException']
        test_method_3.compartment_id = "TEST_3"
        test_method_3.propagate = ['Exception']
        test_class_3.name = 'Test3'
        test_class_3.methods = [test_method_3]
        test_class_3.compartment_id = "TEST_3"

        test_class_4 = JavaClass()
        test_method_4 = JavaMethod('testMethod4', [], ['private'], 'String')
        test_method_4.handle = ['Exception']
        test_method_4.compartment_id = "TEST_4"
        test_class_4.name = 'Test4'
        test_class_4.methods = [test_method_4]
        test_class_4.compartment_id = "TEST_4"

        program['com.test.package'] = [test_class_1, test_class_2, test_class_3, test_class_4]

        rp = RuleEngine(program)
        violations = rp.must(test_input)
        self.failUnless('testMethod1' in violations)
        self.failIf('testMethod2' in violations)
        self.failIf('testMethod3' in violations)
        self.failUnless('testMethod4' in violations)

        # TEST_3 must remap ABCException;
        test_input = [('TEST_3', 'remap', 'ABCException, '),
                      ('TEST_2', 'propagate', 'ABCException, FooBarException')]

        violations = rp.must(test_input)

        self.failUnless('testMethod3' in violations)
        self.failIf('testMethod1' in violations)
        self.failIf('testMethod2' in violations)
        self.failIf('testMethod4' in violations)

        # TEST_3 must raise IOException
        test_input = [('TEST_3', 'raise', 'IOException')]

        violations = rp.must(test_input)

        self.failIf('testMethod1' in violations)
        self.failIf('testMethod2' in violations)
        self.failIf('testMethod3' in violations)
        self.failIf('testMethod4' in violations)

    def test_may_only_rule(self):
        # The TEST compartment may only handle IOException,ABCException and FooBarException
        test_input = [('TEST_1', 'handle', 'IOException, RuntimeException')]

        # The test program
        program = {}

        test_class_1 = JavaClass()
        test_method_1 = JavaMethod('testMethod1', [], ['public'], 'void')
        test_method_1.throw = ['XYZException']
        test_method_1.compartment_id = "TEST_1"
        test_method_1.remap = ['ABCException']
        test_class_1.name = 'Test1'
        test_method_1.handle = ['IOException', 'ABCException', 'RuntimeException']
        test_class_1.methods = [test_method_1]
        test_class_1.compartment_id = "TEST_1"

        test_class_2 = JavaClass()
        test_method_2 = JavaMethod('testMethod2', [], ['public'], 'void')
        test_method_2.throw = ['FooBarException']
        test_method_2.propagate = ['ABCException', 'FooBarException']
        test_method_2.compartment_id = "TEST_2"
        test_method_2.handle = ['IOException']
        test_class_2.name = 'Test2'
        test_class_2.methods = [test_method_2]
        test_class_2.compartment_id = "TEST_2"

        test_class_3 = JavaClass()
        test_method_3 = JavaMethod('testMethod3', [], ['public'], 'void')
        test_method_3.throw = ['IOException']
        test_method_3.compartment_id = "TEST_3"
        test_method_3.propagate = ['Exception']
        test_method_3.remap = ['FooBarException']
        test_class_3.name = 'Test3'
        test_class_3.methods = [test_method_3]
        test_class_3.compartment_id = "TEST_3"

        test_class_4 = JavaClass()
        test_method_4 = JavaMethod('testMethod4', [], ['private'], 'String')
        test_method_4.handle = ['Exception']
        test_method_4.compartment_id = "TEST_4"
        test_class_4.name = 'Test4'
        test_class_4.methods = [test_method_4]
        test_class_4.compartment_id = "TEST_4"

        program['com.test.package'] = [test_class_1, test_class_2, test_class_3, test_class_4]

        rp = RuleEngine(program)
        violations = rp.may_only(test_input)
        self.failUnless('testMethod1' in violations)
        self.failIf('testMethod2' in violations)
        self.failIf('testMethod3' in violations)
        self.failIf('testMethod4' in violations)

        # TEST_3 may only remap ABCException;
        # TEST_2 may only propagate ABCException and FooBarException
        test_input = [('TEST_3', 'remap', 'ABCException'),
                      ('TEST_2', 'propagate', 'ABCException, FooBarException')]

        violations = rp.may_only(test_input)

        self.failUnless('testMethod3' in violations)
        self.failIf('testMethod1' in violations)
        self.failUnless('testMethod2' in violations)
        self.failIf('testMethod4' in violations)

        # TEST_4 may only raise IOException
        test_input = [('TEST_4', 'raise', 'IOException')]

        violations = rp.may_only(test_input)

        self.failIf('testMethod1' in violations)
        self.failIf('testMethod2' in violations)
        self.failIf('testMethod3' in violations)
        self.failIf('testMethod4' in violations)


    def test_dataset_1(self):
        extractor = Extractor()
        extractor.walk('test_cases/1/')
        se = QueryExecutor(extractor.files)

        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()

        aeu = AssignExceptionUsage(pre_processor_results)
        se.add_multitype_query(aeu)

        se.run()

        program = se.program
         # Run the rule parser
        rule_parser = RuleEngine(program)
        rule_parser.run('test_cases/1/rule_set.epl')

        package = program['com.epl.test1']

        for java_class in package:
            for java_method in java_class.methods:
                self.failIf(len(java_method.throw) > 0)
                self.failIf('FooBarException' not in java_method.rethrow)


    def test_dataset_2(self):
        extractor = Extractor()
        extractor.walk('test_cases/2/')
        se = QueryExecutor(extractor.files)

        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()

        aeu = AssignExceptionUsage(pre_processor_results)
        se.add_multitype_query(aeu)

        se.run()

        program = se.program

         # Run the rule parser
        rule_parser = RuleEngine(program)
        rule_parser.run('test_cases/2/rule_set.epl')

        package = program['com.epl.test2']
        for java_class in package:
            for java_method in java_class.methods:
                self.failIf(len(java_method.rethrow) > 0)
                self.failIf(len(java_method.throw) > 0)
                self.failIf('FooBarException' not in java_method.remap)
                self.failIf('IOException' not in java_method.remap)

    def test_dataset_3(self):
        extractor = Extractor()
        extractor.walk('test_cases/3/')
        se = QueryExecutor(extractor.files)

        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()

        aeu = AssignExceptionUsage(pre_processor_results)
        se.add_multitype_query(aeu)

        se.run()
        program = se.program

         # Run the rule parser
        rule_parser = RuleEngine(program)
        rule_parser.run('test_cases/3/rule_set.epl')

        package = program['com.epl.test3']

        for java_class in package:
            for java_method in java_class.methods:
                self.failIf('FooBarException' not in java_method.throw)
                self.failIf('SQLException' not in java_method.throw)
                self.failIf('LOLException' not in java_method.handle)
                self.failIf('RethrowException' in java_method.handle)
                self.failIf('RethrowException' not in java_method.rethrow)
                self.failIf('RemapException' not in java_method.remap)
                self.failIf('RemapException' in java_method.handle)
                self.failIf('RemapException' in java_method.throw)
                self.failIf('RemapException' in java_method.rethrow)
            for si in java_class.static_initializers:
               self.failIf('StaticInitException' not in si.throw)

    def test_dataset_4(self):
        extractor = Extractor()
        extractor.walk('test_cases/4/')
        se = QueryExecutor(extractor.files)

        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()

        aeu = AssignExceptionUsage(pre_processor_results)
        se.add_multitype_query(aeu)

        se.run()
        program = se.program

         # Run the rule parser
        rule_parser = RuleEngine(program)
        rule_parser.run('test_cases/4/rule_set.epl')

        package = program['com.epl.test4']

        for java_class in package:
            for java_method in java_class.methods:
                self.failIf(len(java_method.rethrow) > 0)
                self.failIf(len(java_method.remap) > 0)
                self.failIf('Exception' not in java_method.propagate)

        violations = rule_parser.only_may([('B', 'propagate', 'Exception')])
        self.failIf('bar' not in violations)

    def test_dataset_5(self):
        extractor = Extractor()
        extractor.walk('test_cases/5/')
        se = QueryExecutor(extractor.files)

        # Add search_engine queries to search_engine engine
        pre_processor_results = se.run_preprocessor()

        aeu = AssignExceptionUsage(pre_processor_results)
        se.add_multitype_query(aeu)

        se.run()
        program = se.program

         # Run the rule parser
        rule_parser = RuleEngine(program)
        rule_parser.run('test_cases/5/rule_set.epl')

        package = program['com.epl.test5']

        violations = rule_parser.cannot([('A', 'propagate', 'IOException')])
        violations += rule_parser.cannot([('B', 'throw', 'StaticInitException')])
        self.failIf('bar' not in violations)
        self.failIf('{}' not in violations)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
