package com.ehf.test1;

/**
 * @author Benjamin Jakobus
 */
public abstract class A {

	static {
        	int a = 5;
    	}

	public void foo() {
		try {
			
		} catch (FooBarException e) {
			bar();
			int b = 6;
			throw e;
		}
	}

}
