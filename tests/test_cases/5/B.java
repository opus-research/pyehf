package com.ehf.test5;

/**
 * @author Benjamin Jakobus
 */
public abstract class B {

    static {
        	int a = 5;
		if (a < 5) {
			for (int i = 0; i < 10; i++) {
				while (true) {
					throw new StaticInitException();
				}
			}
		}

    	}

	public void foo() throws Exception {
	    A a = new A();
		a.bar();
	}

}
