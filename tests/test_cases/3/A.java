package com.ehf.test3;

/**
 * @author Benjamin Jakobus
 */
public abstract class A {

	static {
        	int a = 5;
		if (a < 5) {
			for (int i = 0; i < 10; i++) {
				while (true) {
					throw new StaticInitException();
				}
			}
		}
		
    	}

	public void bar() {
		if (true) {
			throw new FooBarException();
		}
		try {
			throw new SQLException();		
		} catch (FooBarException e) {
			bar();
			int b = 6;
		} catch (LOLException se) {
		} catch (RethrowException re) {
		    throw re;
		} catch (RemapException rme) {
		    throw new RemapException(rme.getMessage());
		}
	}

}
