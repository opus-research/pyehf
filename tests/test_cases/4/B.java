package com.ehf.test4;

/**
 * @author Benjamin Jakobus
 */
public abstract class B {

	public void foo() throws Exception {
	    A a = new A();
		a.bar();
	}

}
