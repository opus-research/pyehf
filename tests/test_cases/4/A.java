package com.ehf.test4;

/**
 * @author Benjamin Jakobus
 */
public abstract class A {

	static {
        	int a = 5;
		if (a < 5) {
			for (int i = 0; i < 10; i++) {
				while (true) {
					throw new StaticInitException();
				}
			}
		}
		
    	}

	public void bar() throws Exception {
		bar();
	}

}
