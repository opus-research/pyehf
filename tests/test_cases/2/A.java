package com.ehf.test2;

/**
 * @author Benjamin Jakobus
 */
public abstract class A {

	public void bar() {
		try {
			
		} catch (FooBarException e) {
			bar();
			int b = 6;
			throw new FooBarException();
		} catch (SQLException se) {
			throw new IOException();
		}
	}

}
