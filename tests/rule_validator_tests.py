import unittest

from epl.rule_engine import RuleEngine
from epl.rule_validator import RuleValidator
from extractor import Extractor
from java_structs import JavaClass, JavaMethod
from search_engine.queries.assign_exception_usage import AssignExceptionUsage
from search_engine.query_processor import QueryExecutor


__author__ = 'Benjamin Jakobus'


class RuleValidatorTests(unittest.TestCase):

    def test_1(self):
        rules = {'cannot': [('A', 'throw', 'X')],
                 'must': [('B', 'throw', 'Y'), ('A', 'throw', 'X')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_1)' + str(rv.log)
        self.failIf(ret)

    def test_2(self):
        rules = {'cannot': [('T', 'throw', 'X')],
                 'must': [('B', 'throw', 'Y'), ('A', 'throw', 'X')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_2)' + str(rv.log)
        self.failUnless(ret)

    def test_3(self):
        rules = {'cannot': [('A2', 'propagate', 'Y')],
                 'may-only': [('A2', 'propagate', 'Y')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_3)' + str(rv.log)
        self.failIf(ret)

    def test_4(self):
        rules = {'cannot': [('A1', 'propagate', 'Y')],
                 'may-only': [('A2', 'propagate', 'Y')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_4)' + str(rv.log)
        self.failUnless(ret)

    def test_5(self):
        rules = {'only-may': [('X', 'raise', 'Foo'), ('Y', 'raise', 'Foo')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_5)' + str(rv.log)
        self.failIf(ret)

    def test_6(self):
        rules = {'only-may': [('Y', 'raise', 'FooBar'), ('Y', 'raise', 'Foo')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_6)' + str(rv.log)
        self.failUnless(ret)

    def test_7(self):
        rules = {'only-may': [('C', 'raise', 'Foo')],
                 'may-only': [('C', 'raise', 'Bar')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_7)' + str(rv.log)
        self.failIf(ret)

    def test_8(self):
        rules = {'only-may': [('C', 'raise', 'Foo')],
                 'may-only': [('C', 'raise', 'Foo')]}
        rv = RuleValidator()
        ret = rv.run(rules)
        print '(test_8)' + str(rv.log)
        self.failUnless(ret)


def main():
    unittest.main()

if __name__ == '__main__':
    main()
