from zipfile import ZipFile
import zipfile

__author__ = 'Benjamin Jakobus'

from setuptools import setup
from setuptools.command.install import install as _install
import urllib2
import shutil
import os

class install(_install):
    def run(self):
        _install.run(self)

        response = urllib2.urlopen('https://github.com/musiKk/plyj/archive/master.zip')
        zipped = response.read()

        output = open("plyj.zip",'wb')
        output.write(zipped)
        output.close()

        self.extract_archive('plyj.zip')
        try:
            shutil.copytree('plyj-master/plyj/', 'plyj/')
        except Exception:
            print 'ERROR: Could not copy PlyJ'

        for root, dirs, files in os.walk('./plyj'):
            for momo in dirs:
                os.chown(os.path.join(root, momo), 640, 20)
            for momo in files:
                os.chown(os.path.join(root, momo), 640, 20)

        os.remove('plyj.zip')
        shutil.rmtree('plyj-master')

    def extract_archive(self, file_name):
        with zipfile.ZipFile(file_name) as zf:
            zf.extractall('.')
setup(
    name='pyehf',
    cmdclass={'install': install},
    version='1.0',
    author='Benjamin Jakobus',
    author_email='benjamin.jakobus@gmail.com',
    packages=['epl', 'search_engine', 'tests'],
    url='https://bitbucket.org/opus-research/pyehf',
    license='Apache 2.0',
    description='An experimental implementation of EPL - a DSL for specifying exception handling policies',
    long_description=open('README.md').read(),
    install_requires=[
        "ply >= 3.4",
    ],
    test_suite='tests'
)



